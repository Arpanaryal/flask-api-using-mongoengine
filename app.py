# Take a guidelines from : https://stackabuse.com/guide-to-flask-mongoengine-in-python/


from flask import Flask
from flask_mongoengine import MongoEngine
from flask import jsonify, request

from bson.objectid import ObjectId

# To create a flask app opbject
app = Flask(__name__)

# To config to database
app.config["MONGODB_HOST"]="mongodb+srv://fuse:fuse@sandbox.gpgrg.mongodb.net/Cars?retryWrites=true&w=majority"

# To initialize mongo engine object
db = MongoEngine(app)

# Class model
class Car(db.Document):
    brand = db.StringField(required = True)
    cc = db.IntField(required = True)
    maxspeed = db.IntField(required = True)

#  Mongoenginge doesn't require constructure : reason(https://stackoverflow.com/questions/38363726/mongoengine-attributeerror)
    # def __init__(self, brand, cc, maxspeed):
    #     self.brand = brand
    #     self.cc = cc
    #     self.maxspeed = maxspeed




# To get all of them

@app.route('/car', methods = ['GET'])
def get_cars():
    cars = Car.objects()
    return jsonify(cars), 200

# To create the document of car
@app.route('/car', methods=['POST'])
def add_car():
    body = request.get_json()
    car = Car(**body).save()
    return jsonify(car), 201

#  To get the document of single car

@app.route('/car/<id>', methods = ['GET'])
def get_car(id):
    car = Car.objects(id=ObjectId(id)).first()
    return jsonify(car), 200



# To update a document of a car
@app.route('/car/<id>', methods =['PUT'])
def update_car(id):
    body = request.get_json()
    car = Car.objects.get_or_404(id=ObjectId(id))
    car.update(**body)
    return jsonify(str(car.id)), 200

# To delete any document of a car

@app.route('/car/<id>', methods =['DELETE'])
def delete_car(id):
    car = Car.objects.get_or_404(id=ObjectId(id))
    car.delete()
    return jsonify(str(car.id)), 200



# To run server runserver
if __name__== "__main__" :
    app.run(debug = True)

